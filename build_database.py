
from src import OsmDownloader

search_terms = [
    'water=reservoir', 
    'landuse=reservoir', 
    'water=lake'
]
tile_size = 5
downloader = OsmDownloader(search_terms, 'downloads/overpass-downloaded', tile_size)
downloader.threaded_download()
