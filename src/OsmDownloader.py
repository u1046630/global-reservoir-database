#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 11:00:26 2020

@author: liam
"""

import requests, os, progressbar, random, time, queue, threading

# Overpass API servers, can round robin to reduce server load
urls = [
    'https://overpass-api.de/api/interpreter',
    'http://overpass.openstreetmap.fr/api/interpreter',
    'https://overpass.kumi.systems/api/interpreter',
    'https://overpass.nchc.org.tw/api/interpreter',
]

base_query = '''
[out:json];
(
{snips}
);
out body;
>;
out skel qt;
'''   

# small pieces of Overpass queries that can be combined together
query_snips = {
    'water=reservoir': 
        'way["natural"="water"]["water"="reservoir"]({bbox});\n' + 
        'relation["natural"="water"]["water"="reservoir"]({bbox});',
    'landuse=reservoir':
        'way["landuse"="reservoir"]({bbox});\n' + 
        'relation["landuse"="reservoir"]({bbox});',
    'water=lake':
        'way["water"="lake"]({bbox});\n' + 
        'relation["water"="lake"]({bbox});',
}

class OsmDownloader(object):
    
    def __init__(self, search_terms, cache_dir, tile_size, url=None):
        self.url = url
        self.request_id = 0
        
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)
        self.cache_dir = cache_dir
        
        snip_list = []
        for term in search_terms:
            if term in query_snips:
                snip_list.append(query_snips[term])
            else:
                raise Exception(f'[!] I don\'t have a query snip for the search term "{term}"')
        self.query = base_query.format(snips='\n'.join(snip_list))
        
        if 180 % tile_size != 0:
            raise Exception('[!] tile_size must divide 180')
        self.tile_size = tile_size
        self.tiles = []
        for lon in range(-180, 180, tile_size):
            for lat in range(-90, 90, tile_size):
                self.tiles.append((lat,lon))
        random.shuffle(self.tiles)        
        
        
    def download_all(self):
        s = self.tile_size
        for lat, lon in progressbar.progressbar(self.tiles):
            
            fname = os.path.join(self.cache_dir, f'{lat},{lon},{lat+s},{lon+s}.json')
            if os.path.exists(fname):
                continue
            
            url = self.url
            if url is None:
                url = urls[self.request_id % len(urls)]
            
            bbox='{},{},{},{}'.format(lat, lon, lat+s, lon+s)
            query = self.query.format(bbox=bbox)
            
            t0 = time.time()
            response = requests.get(url, params={'data': query})
            print('\n[*] request time: {:.1f}s from {}'.format(time.time() - t0, url), flush=True)
            response.raise_for_status()
            with open(fname, 'wb') as f:
                f.write(response.content)   
                
            self.request_id += 1
            
            
    def threaded_download(self):
        
        q_in = queue.Queue() # give jobs to workers (lat,lon) tuple
        q_out = queue.Queue() # workers signal job is done
        
        threads = []
        for url in urls:
            thread = threading.Thread(target=OsmDownloader.worker, args=(self, url, q_in, q_out))
            thread.daemon = True #?
            thread.start()
            
        for lat, lon in progressbar.progressbar(self.tiles):
            q_out.get()
            q_in.put((lat, lon))
        for thread in threads:
            thread.join()
        
    def worker(self, url, q_in, q_out):
        q_out.put('done')
        while True:
            job = q_in.get()
            if job is None:
                break
            lat,lon = job
            self.request(url, lat, lon)
            q_out.put('done')
        
    def request(self, url, lat, lon):
        s = self.tile_size
        fname = os.path.join(self.cache_dir, f'{lat},{lon},{lat+s},{lon+s}.json')
        if os.path.exists(fname):
            return
        bbox='{},{},{},{}'.format(lat, lon, lat+s, lon+s)
        query = self.query.format(bbox=bbox)
        response = requests.get(url, params={'data': query})
        if response.status_code == 429: # Too many requests:
            print(f'\n[*] Too many requests ({url})')
            time.sleep(10)
            return
        elif response.status_code == 504:
            print(f'\n[*] Gateway timeout ({url})')
            return
        elif response.status_code != 200:
            print(f'\n[!] Status code {response.status_code} ({url})')
        with open(fname, 'wb') as f:
            f.write(response.content)
        
            
            
    