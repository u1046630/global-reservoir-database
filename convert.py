#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 13:21:59 2020

@author: liam

Check that the JSON downloaded from Overpass is valid.
Then convert it to GeoJSON format.
"""

import glob, os, json, progressbar, threading, queue

num_workers = 2

def worker(q_in, q_out):
    q_out.put(('success', None))
    while True:
        path_from = q_in.get()
        if path_from == None:
            break
            
        name = os.path.basename(path_from).split('.json')[0]
        path_to = f'downloads/geojson-converted/{name}.geojson'
        if os.path.exists(path_to):
            q_out.put(('skipped', path_to))
            continue
        try:
            with open(path_from, 'r') as f:
                json.loads(f.read())
        except:
            q_out.put(('error', path_from)) # Invalid JSON
            continue
        
        os.system(f'osmtogeojson {path_from} > {path_to}')
        q_out.put(('success', path_from))

q_in = queue.Queue()
q_out = queue.Queue()

threads = []
for i in range(num_workers):
    thread = threading.Thread(target=worker, args=(q_in, q_out))
    thread.start()
    threads.append(thread)
    
globs = list(glob.glob('downloads/overpass-downloaded/*.json'))
errors = []
for path_from in progressbar.progressbar(globs):
    result = q_out.get()
    if result[0] == 'error':
        errors.append(result[1])
    q_in.put(path_from)
        
print('[*] invalid JSONs:')
print('\n'.join(errors))
    