
# Lake and Reservoir data sources

## 1. Open Street Maps

natural=water + water=reservoir
natural=water + water=lake
landuse=reservoir


# Conversion options

## 1. From Overpass

Download from Overpass API, use osmtogeojson library (used by overpass turbo)
https://wiki.openstreetmap.org/wiki/Overpass_turbo/GeoJSON
https://github.com/tyrasd/osmtogeojson

## 2. From PBF

QGIS opens .osm.pbf (Openstreet maps Protocolbuffer Binary Format) and can save it as shapefile or geojson
https://wiki.openstreetmap.org/wiki/Converting_map_data_between_formats

## 3. From PBF

Using the command line programme ogr2ogr:
Load OSM data into PostgreSQL database using Osm2pgsql or Imposm.
Convert data using ogr2ogr where ogr2ogr reads from the database and writes to your desired output format.
https://wiki.openstreetmap.org/wiki/Converting_map_data_between_formats
