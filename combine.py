#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 14:03:34 2020

@author: liam

Combine all the GeoJSON files into one big shapefile
while removing any polygons that are smaller than 1 ha. 
"""

import geopandas as gpd, pyproj, progressbar, glob, os

geod = pyproj.Geod(ellps='WGS84')
min_area = 0.01

# distance in meters between two points in WGS84
def dist(lat1, lon1, lat2, lon2):
    return geod.inv(lon1, lat1, lon2, lat2)[2]

# area in sq km of polygon defined on WGS84 coordinates
def calc_size(poly):
    c = poly.centroid
    lat,lon = c.y, c.x
    # conversion factor: square km per square lat/lon degree
    multiplier = dist(lat-0.05, lon, lat+0.05, lon) * dist(lat, lon-0.05, lat, lon+0.05)
    multiplier = multiplier * 10 * 10 / 1000 / 1000
    return poly.area * multiplier


globs = list(glob.glob('downloads/geojson-converted/*.geojson'))
for fname in progressbar.progressbar(globs):
    if os.stat(fname).st_size == 0:
        with open(fname_output, 'w') as f:
            f.write('')
        continue
    name = os.path.basename(fname).split('.geojson')[0]
    fname_output = f'downloads/shapefile-converted/{name}.shp'
    if os.path.exists(fname_output):
        continue
    gdf = gpd.read_file(fname)
    if len(gdf) == 0:
        with open(fname_output, 'w') as f:
            f.write('')
        continue
    areas = []
    for poly in gdf.geometry:
        areas.append(calc_size(poly))
    gdf['area'] = areas
    gdf = gdf[gdf.area > min_area]
    if len(gdf) == 0:
        with open(fname_output, 'w') as f:
            f.write('')
        continue
    gdf.to_file(fname_output)
    